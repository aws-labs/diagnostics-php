import {client} from "../../Utils/js/internal/dnsimple";

describe("dnsimple", ()=>{
    test("is domain name given/set?", ()=>{
        expect(client.client.baseUrl.length).toBeGreaterThanOrEqual(0);
    });

    test("is client a function?", ()=>{
       expect(typeof client).toBe('function');
    });
});