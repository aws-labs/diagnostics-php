import {aa, spfFunction} from "../../Utils/js/external/spf";
import {a} from "../../Utils/js/math";

describe("SPF test group", ()=>{
   test("is properly defined", ()=>{
       expect(spfFunction).toBeFunction;
   });

    test("is SPF empty?", ()=>{
        expect(spfFunction.prototype.spf).not.toBeEmpty;
    });

    test("What is SPF?", ()=>{
        const spfmockvalue = jest.mock(spfFunction.prototype.spf);
        expect(spfmockvalue).toBe.length > 0;
    });

    test("Is spffunction an object?", ()=>{
        const spfmockvalue = jest.mock(spfFunction.prototype.spf);
        expect(spfmockvalue).toBeObject;
    });

    // @todo: investigate Syntax error. Seems broken upstream.
   /* test("Is aafunction an object??", ()=>{
        const fvf = jest.mock(spfFunction.prototype);
        expect(aafunction).toBeObject;
    });*/
});