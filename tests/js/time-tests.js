import * as tt from '../../Utils/js/internal/timeTracking';
import * as ui from '../../Utils/js/internal/userinput';

/**
 * @deprecated
describe("time series tests", ()=>{
    test("page load time as function", ()=>{
        expect(t.pageloadtime).toBeFunction;
    });

    test("page load is a number", ()=>{
        expect(t.pageloadtime).not.toBeNumber;
    });

    test("page load is not negative", ()=>{
        expect(t.pageloadtime).toBeGreaterThan(0);
    });
});
**/

jest.mock('../../Utils/js/timeTracking', () => ({
    __esModule: true,
    openThisWeb: jest.fn(() => 1)
}));

/**
 * @default
 */
describe("timeTracking tests", () => {
   test("method 1 as function", ()=>{
       expect(tt.withTimeout).toBeFunction;
   });

    test("method 2 as function", ()=>{
        expect(tt.withStartStopTimers).toBeFunction;
    });

    test("method 1 NOT to be empty", ()=>{
        const isdef = tt.withTimeout;
        expect(isdef).not.toBeEmpty;
    });

    test("method 2 is defined", ()=>{
        expect(tt.withStartStopTimers).toBeFunction;
    });
});