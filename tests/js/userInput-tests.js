/*
describe("Username input tests", ()=>{
    test("test function  calls", ()=>{
       const mockedUIT = jest.fn(openThisWeb);
       jest.mock('../../Utils/js/userinput', ()=> {
           __esmodule: true,
               userinput: jest.fn(() => 'Mocked')
       });
   });
});*/

describe('Overview Test', () => {

    // Mock the module and its functions
    jest.mock('../../Utils/js/userinput', () => ({
        __esModule: true,
        openThisWeb: jest.fn(() => 1)
    }));

    // Import the function from the mocked module
    const {openThisWeb: openThisWeb} = require('../../Utils/js/internal/userinput');
    const returnValue = openThisWeb();

    test('snapshot', () => {
        // Execute the mocked function
        // Expect to return the mocked value
        expect(returnValue).toBe(typeof returnValue === 'string');
    });

});
