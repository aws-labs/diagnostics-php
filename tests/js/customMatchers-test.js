import * as format from "../../Utils/js/jest/customJestMatchers";
import message from '../../Utils/js/jest/customJestMatchers';

describe("customJestMatchers tests", ()=>{
    const mock = jest.fn();
    const results = 6;

    test("function returns what it needs to return", ()=>{
       expect(results).toBe(6);
    });

    /**
     * @deprecated
     * @todo: refactor
     */
    /*test("spy test", ()=>{
        const spy = jest.spyOn(format, "function");
        const getResults = () => Promise => spy.mock.results.value;
        expect(getResults).toBe(6);
    });*/
    test("has message get value it should get?", ()=>{
        expect(message).not.toBeEmpty;
    });
});