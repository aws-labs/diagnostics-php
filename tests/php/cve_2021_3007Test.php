<?php
declare(strict_types=1);

namespace php\utils\libs\cve;

use diagnosticsphp\utils\libs\cve\cve_20213007_fixclass;
use PHPUnit\Framework\TestCase;

final class cve_2021_3007Test extends TestCase
{
    public function testTearDown()
    {
        $cve = new cve_20213007_fixclass();
        $stub = $this->createStub(cve_20213007_fixclass::class);
        $stub->method($stub->tmpFile)
            ->willReturn(true);
    }

    public function testSetUp()
    {
        $cve = new cve_20213007_fixclass();
        $stub = $this->createStub(cve_20213007_fixclass::class);
        $stub->method($stub->setupEnabled);
    }
}
