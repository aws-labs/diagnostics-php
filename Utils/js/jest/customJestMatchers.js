import prettier from 'prettier'
import diff from 'jest-diff'

/**
 *
 * @param input
 * @returns {string}
 */
export function format(input) {
    return prettier.format(input, {
        parser: 'css',
        printWidth: 100,
        arrowParens: "avoid",
        insertPragma: true,
        proseWrap: "preserve",
        quoteProps: "consistent"
    })
}

/**
 * @since 0.2-dev
 */
expect.extend({
    toMatchCss(received, argument) {
        function stripped(str) {
            return str.replace(/\s/g, '').replace(/;/g, '')
        }

        const options = {
            comment: 'stripped(received) === stripped(argument)',
            isNot: this.isNot,
            promise: this.promise,
            message: "PatternMatcher returned "+received.toString()
        }

        const pass = stripped(received) === stripped(argument)
        const message = pass
            ? () => {
                return (
                    this.utils.matcherHint('toMatchCss', undefined, undefined, options) +
                    '\n\n' +
                    `Expected: not ${this.utils.printExpected(format(received))}\n` +
                    `Received: ${this.utils.printReceived(format(argument))}`
                )
            }
            : () => {
                const actual = format(received)
                const expected = format(argument)

                const diffString = diff(expected, actual, {
                    expand: this.expand,
                })

                return (
                    this.utils.matcherHint('toMatchCss', undefined, undefined, options) +
                    '\n\n' +
                    (diffString && diffString.includes('- Expect')
                        ? `Difference:\n\n${diffString}`
                        : `Expected: ${this.utils.printExpected(expected)}\n` +
                        `Received: ${this.utils.printReceived(actual)}`)
                )
            }
        return { actual: received, message, pass }
    },
})