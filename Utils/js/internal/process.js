/*
 * Copyright (c) 2021. awslabspl
 */
const logger = require('./projectLoggerUtility');
export var uncaughtExceptionFunction =
    process.on('uncaughtException', error => {
        logger.logCiticalError(error.statusCode);
        process.exit(1);
    });

export var uncaughtExceptionUID = uncaughtExceptionFunction.geteuid();

// Log UID value
console.log(uncaughtExceptionUID);