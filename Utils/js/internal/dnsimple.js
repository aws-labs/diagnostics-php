"use strict";
export var client = require('dnsimple')({
    accessToken: process.env.TOKEN,
    baseUrl: "https://api.sandbox.dnsimple.com",
});

client.identity.whoami().then(function(response) {
    console.log(response.data);
}, function(error) {
    console.log(error);
});

// List your domains
export var accountId = "1010";
client.domains.listDomains(accountId).then(function(response) {
    console.log(response.data);
}, function(error) {
    console.log(error);
});

client.domains.listDomains(accountId, { page: 3 }).then(function(response) {
    console.log(response.data);
}, function(error) {
    console.log(error);
});

// Create a domain
client.domains.createDomain(accountId, { name: "example.com" }).then(function(response) {
    console.log(response.data);
}, function(error) {
    console.log(error);
});

// Get a domain
client.domains.getDomain(accountId, "example.com").then(function(response) {
    console.log(response.data);
}, function(error) {
    console.log(error);
});