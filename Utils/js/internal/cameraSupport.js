import {logCiticalError} from "./projectLoggerUtility";

let params = {
    audio: true,
    video: {
        facingMode: {
                exact: "environment"
        }
    }
};

if (navigator.mediaDevices.getUserMedia){
    navigator.mediaDevices.getUserMedia({video: true})
        .then(function (stream){
            video.srcObject = stream;
        }).catch(function (errOr) {
            logCiticalError("Something is wrong");
    });
}

let blobs = [];
let stream, mediaRecorder, blob;

async function startRecording() {
    stream = await navigator.mediaDevices.getUserMedia({
        audio: true,
        video: true
    });

    mediaRecorder = new mediaRecorder(stream);
    mediaRecorder.ondataavailable = (event) => {
        if (event.data){
            blobs.push(event.data);
        }
    };
    mediaRecorder.onstop = doPreview;
    mediaRecorder.start(1000);
}

function endRecording() {
    mediaRecorder.stop();
    stream.getTracks().forEach((track) => track.stop());
}