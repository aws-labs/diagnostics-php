var requirejs = require('requirejs');

require.config({
    paths:{
        mainIndexFile: '../../../index.html',
        processDefinitionFileName: './process.js',
        processTestFile: '../../../tests/js/process-test.js'
    }
});