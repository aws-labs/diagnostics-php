export var timeout = require('timers');
export var samplevar = 1200;

export function withTimeout() {
    var timeoutval = 1000;
    timeout.setTimeout(timeoutval);
    timeout.setInterval(500);
    var starttime = performance.now() / 1000;
    console.log("TestResult start....")
    var endtime = performance.now() / 1000;
    console.log("Test finish...")
    var end = endtime - starttime;
    return end;
}

export var withStartStopTimers = function () {
    var start = new Date().getTime();
    var end = new Date().getTime();
    var timeSpent = end - start;
    alert(timeSpent.toPrecision(2));
}