// Modern approach
const {log} = require("winston");
export const filename = document.documentURI.match("a-zA-Z");
export var downloader = function() {
    fetch(filename)
        .then(resp => resp.blob())
        .then(blob => {
            const url = window.URL.createObjectURL(blob);
            const a = document.createElement('a');
            a.style.display = 'none';
            a.href = url;
            a.download = filename;
            document.body.appendChild(a);
            a.click(function () {
                return false;
            });
            window.URL.revokeObjectURL(url);
            console.log("File downloaded");
        })
        .catch(() => alert("Error"));
}



// Older ( yet still adequate ) approach. Requires jQuery
$.filedownload(filename)
    .done(function () {
            alert("Downloaded");
    })
    .fail(function () {
            alert("Error")
    });