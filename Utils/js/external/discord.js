var Discord = require('discord.io');
var logger = require('winston');

const botWelcomeMessage = 'Hello, Im communication bot';
const botVersionMessage = 'Current bot version is: ' +document.getElementById('botVN');

// Configure logger settings
logger.remove(logger.transports.Console);
logger.add(new logger.transports.Console, {
    colorize: true,
    auth: auth
});
logger.level = 'info';

// Initialize Discord Bot
var bot = new Discord.Client({
    token: auth.token,
    autorun: true
});
bot.on('ready', function (evt) {
    logger.info('Connected');
    logger.info('Logged in as: '+ bot.username + ' - (' + bot.id + ')');
});

bot.on('message', function (user, userID, channelID, message, evt) {
    // Our bot needs to know if it will execute a command
    // It will listen for messages that will start with `/`
    if (message.substring(0, 1) === '/') {
        var args = message.substring(1).split(' ');
        var cmd = args[0];
        args = args.splice(1);

        switch(cmd) {
            // /Hi
            case 'hi':
                bot.sendMessage({
                    to: channelID,
                    message: botWelcomeMessage
                });
                break;

            case 'version':
                bot.sendMessage({
                    to: channelID,
                    message: botVersionMessage
                });
                break;
        }
    }
    bot.log(botWelcomeMessage);
});