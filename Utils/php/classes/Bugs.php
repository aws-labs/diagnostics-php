<?php


namespace diagnosticsphp\utils\libs;


use DateTime;

final class Bugs
{
    private int $count;
    private int $sysToken;
    private array $token_pool;
    private array $severity;
    private false|DateTime $creationDate;
    private false|DateTime $modifDate;
    private string $assignee;
    private string $author;
    private int $attId;
    private int $attSize;
    private string $attFileName;
    private bool $readyToBeSent;
    private $file;
    private bool $isAttNeeded;
    //
    public int $bugId;
    public string $bugName;
    public string $bugShortDesc;
    public string $productName;
    public array $priority;
    public array $status;
    public array $bugFormElements;
    public string $bugWebAddr;

    public function __construct(){
        $bsto = new bugSystemOptions();
        $this->creationDate = DateTime::createFromFormat('j-M--Y', date('d-M-Y'));
        $this->modifDate = DateTime::createFromFormat('j-M-Y', $this->creationDate);
        $this->assignee = "Wojciech Lisik";
        $this->author = $_SERVER['SERVER_ADMIN'];
        $this->token_pool = $bsto->tokenPool;
        $this->sysToken = $bsto->token;
        $this->bugId = rand(0, 256);
        $this->bugName = "";
        $this->bugShortDesc = "";
        $this->productName = "diagnostics";
        $this->priority = [
            "P1", "P2", "P3", "P4"
        ];
        $this->severity = [
            "minor", "mild", "high", "critical", "stopper"
        ];
        $this->status = [
            "Unconfirmed","Assigned", "Confirmed", "Being worked on", "Solved", "Done", "Merged", "Fixed"
        ];
        $this->bugFormElements = [
            $this->assignee, $this->author, $this->bugName, $this->bugShortDesc, $this->priority, $this->severity,
            $this->status
        ];
        $this->bugWebAddr;
        $this->count = count($this->bugFormElements);
        $this->attId = rand(0,1024);
        $this->attFileName = $_SERVER['SCRIPT_FILENAME'];
        $this->attSize = filesize($this->attFileName);
        $this->readyToBeSent = false;
        $this->file;
        $this->isAttNeeded = $_GET['attachment'];
        $this->prepareForm();
    }

    function prepareForm(){
        $i = 0;
        for ($i; $this->count < $i; $i++){
            if (!is_null($this->bugFormElements[$i])){
                $this->bugFormElements[$i] = htmlspecialchars($this->bugFormElements[$i]);
                $this->addAttachment();
            } else {
                die("Niepoprawny format danych.");
            }
        }
        return true;
    }

    /**
     * Uploads an attachment to Amazon S3 storage
     *
     * @return mixed
     */
    protected function addAttachment(): mixed
    {
        if (isset($this->isAttNeeded)){
            if (file_exists($this->attFileName)){
                $this->file = fopen($this->attFileName, 'r');
                exec('s3cmd put '.$this->file);
            } else {
                die("No file exists or no file selected");
            }
        }
        return $this->isAttNeeded;
    }

    function closeBug($bugId){
        $this->bugId = $bugId;
        for ($bugId; $bugId < rand(0, 256); $bugId++){
            if (!isset($this->status[$bugId])){
                $this->status[$bugId] = "Unconfirmed";
            } else {
                // TBW
            }
        }
    }
    /*public function generateBugId(){
        return $this->bugId;
    }

    public function setBugWebAddr(string $bugWebAddr): void
    {
        $this->bugWebAddr = $bugWebAddr;
    }*/
}