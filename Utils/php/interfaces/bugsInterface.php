<?php


namespace diagnosticsphp\utils\libs;


interface bugsInterface
{
    function setBugID();
    function setBugDetails();
    function setStatus();
    function setWebAccessibility();
}