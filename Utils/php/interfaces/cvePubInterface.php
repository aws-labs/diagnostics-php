<?php

namespace diagnosticsphp\utils\libs;

interface cveSetupInterface
{
    public function setDetails();
    public function setPublicLink();
    public function uploadCVE();
    public function checkUploadStatus();
    //
    public function getDetails();
    public function getPublicLink();
}